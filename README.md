# The Common Geometry Module (CGM)

The Common Geometry Module (CGM) is a code library ([BitBucket repository](https://bitbucket.org/fathomteam/cgm)) which provides geometry functionality used for mesh generation and other applications.  This functionality includes that commonly found in solid modeling engines, like geometry creation, query and modification; CGM also includes capabilities not commonly found in solid modeling engines, like geometry decomposition tools and support for shared material interfaces.  CGM is not specifically built on specific solid modeling engines, and hence includes abstractions for geometry capability developed by engines such as OCC representation.  CGM can be used as-is to provide geometry functionality for codes needing this capability.  However, CGM can  also be extended using derived classes in C++, allowing the geometric  model to serve as the basis for other applications, for example mesh generation.  CGM is supported on Sun Solaris, SGI, HP, Linux and Windows NT platforms.  CGM also includes support for loading models on parallel computers, using MPI-based communication.  Future plans for CGM are to port it to different solid modeling engines, including Pro/Engineer or SolidWorks.  

## Dependencies

CGM can be built without using any external solid modeling engines; in
this form, facet-based geometry can be constructed, queried and
modified.  Specific engines relying on third-party, commercial
modelers are also available, to those licensed to use those modelers.
Contact the CGM team at cgma-dev_at_mcs.anl.gov for details.

The major (optional) solid modeling dependency that is recommended to most users is the OpenCascade engine. We suggest our users to download and configure the [OpenCascade Community Edition (OCE)](https://github.com/tpaviot/oce/) version. Note that OCE places the include files into \$INCLUDE_DIR/oce, while CGM searches for include files in the \$INCLUDE_DIR. This can be done by specifying \$OCE_INSTALL_INCLUDE_DIR. It might also be required to set \$OCE_INSTALL_PREFIX. From CGM v14.1, the configuration has been modified to look at both \$INCLUDE_DIR and \$INCLUDE_DIR/oce.

CGM can optionally be built using the CMake utility, which is "an extensible,
open-source system that manages the build process in an operating
system and compiler independent manner", or (on *NIX) using a
configure-make system.  CMake is available under an open source
license, and can be downloaded in binary or source formats; see
http://www.cmake.org for details.

## Configuration

### Where's the configure script?

  After installing the GNU autotools suite, execute the following
  command to generate the configure script (and other necessary
  generated files):
  
    autoreconf -fi
    
  If for some reason, the autoreconf command is not available,
  the following sequence of commands should have the same result:
    
    autoheader
    aclocal
    libtoolize
    automake -a
    autoconf 
    

* Why isn't the configure script and other generated files in the
Git repository?
  
>  Because they are generated files.  Why save a version history for
  them?  Further, some of the above commands get re-run automatically
  when Makefile.am's or other files are changed.  This could lead to 
  compatibility problems if some of the generated files in the Git 
  repository are from a different version of the GNU autotools.
  

* Aren't we requiring users to have GNU autotools installed in order 
to configure CGM?

>  No.  Developers (or anyone else using source directly from the Git
  repository) must have the autotools installed.  When creating a
  tarball for distribution of CGM, the commands below should be run.
  The resulting tarball will contain all necessary generated files,
  including the configure script.

* What needs to be done to prepare CGM for distribution as a tarball?

  Check out a clean copy of CGM.
  Execute the following commands in the top-most directory:
  
      autoreconf -fi
      ./configure
      make dist


## Compiling

1. Unpack the source code in some directory CGM_DIR, and change
directory into CGM_DIR.
2a. Run cmake in that directory, using either the non-interactive
("cmake .") or interactive ("ccmake .") version, OR
2b. Run "./configure" in that directory, optionally specifying where
OCC libraries are located and other options (run "./configure -h" for
a list of options).
3. Make the CGM libraries by typing "make".
4. Test the build using "make test".
5. Install the CGM libraries by typing "make install" (this installs
the libraries to the location specified by the --prefix option to
configure, or to /usr/local by default).
6. Test the installation using "make test".

## Documentation, Testing, Example Applications

Example applications are included with the CGM distribution in the
cgm_apps subdirectory.  Examples include importing and querying solid
model-based geometry, and constructing facet-based geometry.  If you
are interested in contributing examples to be included with CGM, we
would welcome submissions; please send them to the email address
below.

The best example for how to build applications is to look at the
cgm_apps/examples/facetdriver subdirectory.

## Bugs, Correspondence, Contributing

CGM is LGPL code, and we encourage users to submit bug reports (and,
if desired, fixes) to cgma-dev_at_mcs.anl.gov. 

# OpenCascade (OCC/OCE) Support

CGM can be built with the OCC solid modeling engine, but also includes geometry capability developed beside and on top of them. CGM is supported on Sun Solaris, SGI, HP, Linux and Windows NT platforms. CGM also includes support for loading OCC models on parallel computers. 

## Build OCC-based engine

* Please download Opencascade Community Edition (OCE) 0.17.1 source package at
  https://github.com/tpaviot/oce/releases/tag/OCE-0.17.1

* Then follow the link instruction to build OCC libraries.
  http://sigma.mcs.anl.gov/cgm/building-cgm/

* Download CGM trunk files, then under trunk do 
  * autoreconf -fi
        * ./configure --with-occ=(your occ dir, parent dir of ros) --prefix=Your install dir.
        * make or make install
  * make check to see some examples of the testcases running.

## Build CGM with older OpenCascade versions (before OCC 6.6 and OCE 0.17.1)

Go to http://www.opencascade.org/getocc/download/loadocc/ and get 
desired source files.

Untar the files.  Change directory to OpenCASCADE6.3.0.
Patch OCC by doing:

  patch -p0 <cgm_source>/geom/OCC/occ_patches

This will update the OCC distribution.
Change directories to ros.  Build OCC by doing:

  autoreconf -fi
  ./configure (Add --prefix=dir for your install directory)
  make 
  make install

## Some CGM capabilities

1.  Query

* Import/Export OpenCascade  b-rep files, create CGM geometry/topology entities.
* Import/Export OpenCascade b-rep plus CGM attribute files.
* Delete entities build on OCC engine.
* Translate/Rotate/Scale/Reflect objects.
  * Added scale with 3 different scale factors in 3 axes.
* Detect body overlap
* Get iso-parametric points on surfaces and curves.
* Get curve-curve, curve-surface intersection points.
* Get distance between entities.
* Get bounding boxes for entities.
* Get facets info for surfaces and curves.

2.  Modify

* Create CGM geometry entities using OpenCascade engine. Top-Down method: directly create points, curves, surfaces or solid primitives.
* Create CGM geometry entities using OpenCascade engine, Bottom-Up method: curve generates by points, surface generates by curves and solid generates by surfaces.
* Copy body.
* Boolean operations: subtract, unite, intersect.
* Project curve on surface
* Imprint object: body-body imprint, surface-body imprint, curve-surface imprint, point-body imprint, projected curve imprint.
* Make thick body by hollowing existing solid.
* Fillet and Chamber creation.
* tweak-move surfaces and curves.
* Sweep surface or curve along a vector, an edge, perpendicularly or revolutionarily.(alpha testing)
* Section, cuts in half. To see a cross section of the model.
* Chop, cuts body by body, returns outside body, intersect body and leftover body.
* Webcut, three point cutting surface, body cutting tool, mid-plane cutting tool.

