set(cgm_igeom_srcs
  CATag.cpp
  iGeomError.cc
  iGeom_CGMA.cc)

set(cgm_igeom_hdrs
  iGeom.h
  iGeom_f.h
  iGeom_protos.h
  )

set(CGM_IGEOM_BUILD_SHARED_LIBS ${BUILD_SHARED_LIBS})
configure_file(
  "${CMAKE_SOURCE_DIR}/config/CGMIGeomConfigure.h.in"
  "${CMAKE_CURRENT_BINARY_DIR}/CGMIGeomConfigure.h"
  @ONLY)

cgm_add_library(iGeom
  ${cgm_igeom_srcs}
  ${cgm_igeom_hdrs}
  # Generated files:
  "${CMAKE_CURRENT_BINARY_DIR}/CGMIGeomConfigure.h")
target_include_directories(iGeom
  PUBLIC
    "$<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>"
    "$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>"
    "$<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/../src>"
    "$<INSTALL_INTERFACE:include>")
target_link_libraries(iGeom
  PUBLIC
    cgm_geom
    cgm_util
    ${CGM_LIBS}
    cgm_init)
install(
  FILES       ${cgm_igeom_hdrs}
  DESTINATION include
  COMPONENT   Development)
install(
  FILES       "${CMAKE_CURRENT_BINARY_DIR}/CGMIGeomConfigure.h"
  DESTINATION include/cgm
  COMPONENT   Development)

