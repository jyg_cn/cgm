# Don't require GNU-standard files (Changelog, README, etc.)
AUTOMAKE_OPTIONS = foreign

DEFS = $(TEMPLATE_DEFS_INCLUDED) $(LITTLE_ENDIAN)
AM_CPPFLAGS = -I$(top_srcdir)/src/util -I$(top_srcdir)/src/geom/cgm -I$(top_srcdir)/src/util/cgm -I$(top_srcdir)/src/geom/Cholla/cgm -I$(top_builddir)/src/geom/cgm -I$(top_builddir)/src/util/cgm

# The name of the library to build
noinst_LTLIBRARIES = 
if BUILD_CGM
  noinst_LTLIBRARIES += libcgm_cholla.la
endif

# The directory where headers will be installed
libcgm_cholla_la_includedir = $(includedir)

LDADD = ../../util/libcgm_util.la ../libcgm_geom.la

# The non-template sources
libcgm_cholla_la_SOURCES = \
    AllocMemManagersCholla.cpp \
    Cholla.cpp \
    ChollaCurve.cpp \
    ChollaEngine.cpp \
    ChollaEntity.cpp \
    ChollaPoint.cpp \
    ChollaSkinTool.cpp \
    ChollaSurface.cpp \
    ChollaVolume.cpp \
    ChordalAxis.cpp \
    CubitFacet.cpp \
    CubitFacetData.cpp \
    CubitFacetEdge.cpp \
    CubitFacetEdgeData.cpp \
    CubitPoint.cpp \
    CubitPointData.cpp \
    CubitQuadFacet.cpp \
    CubitQuadFacetData.cpp \
    CurveFacetEvalTool.cpp \
    ChollaDebug.cpp \
    FacetDataUtil.cpp \
    FacetEntity.cpp \
    FacetEvalTool.cpp \
    GeoNode.cpp \
    GeoTet.cpp \
    LoopParamTool.cpp \
    PointGridSearch.cpp \
    PointLoopFacetor.cpp \
    TDChordal.cpp \
    TDFacetboolData.cpp \
    TDFacetBoundaryEdge.cpp \
    TDFacetBoundaryPoint.cpp \
    TDGeomFacet.cpp 

# Headers to be installed.  If any file in this list should
# not be installed, move it to the _SOURCES list above.
nobase_libcgm_cholla_la_include_HEADERS = \
    cgm/BoundaryConstrainTool.hpp \
    cgm/Cholla.h \
    cgm/ChollaCurve.hpp \
    cgm/ChollaEngine.hpp \
    cgm/ChollaEntity.hpp \
    cgm/ChollaPoint.hpp \
    cgm/ChollaSkinTool.hpp \
    cgm/ChollaSurface.hpp \
    cgm/ChollaVolume.hpp \
    cgm/ChordalAxis.hpp \
    cgm/CubitFacet.hpp \
    cgm/CubitFacetData.hpp \
    cgm/CubitFacetEdge.hpp \
    cgm/CubitFacetEdgeData.hpp \
    cgm/CubitPoint.hpp \
    cgm/CubitPointData.hpp \
    cgm/CubitQuadFacet.hpp \
    cgm/CubitQuadFacetData.hpp \
    cgm/CurveFacetEvalTool.hpp \
    cgm/FacetDataUtil.hpp \
    cgm/FacetEntity.hpp \
    cgm/FacetEvalTool.hpp \
    cgm/FacetorTool.hpp \
    cgm/FacetorUtil.hpp \
    cgm/GeoNode.hpp \
    cgm/GeoTet.hpp \
    cgm/LoopParamTool.hpp \
    cgm/PointGridSearch.hpp \
    cgm/PointLoopFacetor.hpp \
    cgm/TDChordal.hpp \
    cgm/TDFacetBoundaryEdge.hpp \
    cgm/TDFacetBoundaryPoint.hpp \
    cgm/TDFacetboolData.hpp \
    cgm/TDGeomFacet.hpp \
    cgm/TDDelaunay.hpp \
    cgm/TetFacetorTool.hpp \
    cgm/ChollaDebug.hpp 

# If template defs are included, then the template definitions
# need to be installed with the headers.  Otherwise they need
# to be compiled.
if INCLUDE_TEMPLATE_DEFS
  nobase_libcgm_cholla_la_include_HEADERS += \
    cgm/BoundaryConstrainTool.cpp \
    cgm/FacetorTool.cpp \
    cgm/TetFacetorTool.cpp \
    cgm/FacetorUtil.cpp \
    cgm/TDDelaunay.cpp 
else
  libcgm_cholla_la_SOURCES += \
    cgm/BoundaryConstrainTool.cpp \
    cgm/FacetorTool.cpp \
    cgm/TetFacetorTool.cpp \
    cgm/FacetorUtil.cpp \
    cgm/TDDelaunay.cpp 
endif

libcgm_cholla_la_LIBADD=${CGM_EXT_LDFLAGS}
