project(cgm_facet)

set(facet_srcs
  CubitOctree.cpp
  CubitOctreeCell.cpp
  CubitOctreeGenerator.cpp
  CubitOctreeGeneratorVolumes.cpp
  CubitOctreeNode.cpp
  FacetAttrib.cpp
  FacetAttribSet.cpp
  FacetBody.cpp
  FacetCoEdge.cpp
  FacetCurve.cpp
  #FacetGeometryCreator.cpp
  FacetLoop.cpp
  FacetLump.cpp
  FacetModifyEngine.cpp
  FacetParamTool.cpp
  FacetPoint.cpp
  FacetProjectTool.cpp
  FacetQueryEngine.cpp
  FacetShell.cpp
  FacetSurface.cpp
  FacetboolInterface.cpp
  GridSearchTree.cpp
  OctreeFacetPointData.cpp
  OctreeIntersectionData.cpp
  PST_Data.cpp
  TDOctreeRefEdge.cpp
  TDOctreeRefFace.cpp)

set(facet_headers
  cgm/CubitOctree.hpp
  cgm/CubitOctreeCell.hpp
  cgm/CubitOctreeConstants.hpp
  cgm/CubitOctreeGenerator.hpp
  cgm/CubitOctreeGeneratorVolumes.hpp
  cgm/CubitOctreeNode.hpp
  cgm/FacetAttrib.hpp
  cgm/FacetAttribSet.hpp
  cgm/FacetBody.hpp
  cgm/FacetCoEdge.hpp
  cgm/FacetCurve.hpp
  cgm/FacetGeometryCreator.hpp
  cgm/FacetLoop.hpp
  cgm/FacetLump.hpp
  cgm/FacetModifyEngine.hpp
  cgm/FacetParamTool.hpp
  cgm/FacetPoint.hpp
  cgm/FacetProjectTool.hpp
  cgm/FacetQueryEngine.hpp
  cgm/FacetShell.hpp
  cgm/FacetSurface.hpp
  cgm/FacetboolInterface.hpp
  cgm/GridSearchTree.hpp
  cgm/GridSearchTreeNode.hpp
  cgm/OctreeFacetPointData.hpp
  cgm/OctreeIntersectionData.hpp
  cgm/PST_Data.hpp
  cgm/TDOctreeRefEdge.hpp
  cgm/TDOctreeRefFace.hpp)

cgm_source_interface(all_facet_srcs
  facet_srcs
  facet_headers)

add_library(cgm_facet INTERFACE)
target_sources(cgm_facet
  INTERFACE
    ${all_facet_srcs})
target_include_directories(cgm_facet
  INTERFACE
    "$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>/cgm"
    "$<INSTALL_INTERFACE:include>")
target_link_libraries(cgm_facet
  INTERFACE
    cgm_facetboolstub
    cgm_geom
    cgm_cholla)
cgm_install_headers(${facet_headers})

set_property(GLOBAL APPEND
  PROPERTY cgm_libs
  cgm_facet)
