project(cgm_geom)

set(geom_srcs
  AnalyticGeometryTool.cpp
  AutoMidsurfaceTool.cpp
  BasicTopologyEntity.cpp
  Body.cpp
  BodySM.cpp
  BoundingBoxTool.cpp
  BridgeManager.cpp
  CAActuateSet.cpp
  CADeferredAttrib.cpp
  CAEntityColor.cpp
  CAEntityId.cpp
  CAEntityName.cpp
  CAEntitySense.cpp
  CAEntityTol.cpp
  CAGroup.cpp
  CAMergePartner.cpp
  CAMergeStatus.cpp
  CASourceFeature.cpp
  CAUniqueId.cpp
  CGMApp.cpp
  CGMEngineDynamicLoader.cpp
  CGMHistory.cpp
  Chain.cpp
  CoEdge.cpp
  CoEdgeSM.cpp
  CoFace.cpp
  CoVertex.cpp
  CoVolume.cpp
  CollectionEntity.cpp
  CubitAttrib.cpp
  CubitAttribManager.cpp
  CubitAttribUser.cpp
  CubitPolygon.cpp
  CubitSimpleAttrib.cpp
  Curve.cpp
  CurveOverlapFacet.cpp
  CurveSM.cpp
  CylinderEvaluator.cpp
  DAG.cpp
  DagDrawingTool.cpp
  GSaveOpen.cpp
  GeomDataObserver.cpp
  GeomMeasureTool.cpp
  GeometryEntity.cpp
  GeometryEvent.cpp
  GeometryFeatureEngine.cpp
  GeometryFeatureTool.cpp
  GeometryHealerEngine.cpp
  GeometryHealerTool.cpp
  GeometryModifyEngine.cpp
  GeometryModifyTool.cpp
  GeometryQueryEngine.cpp
  GeometryQueryTool.cpp
  GeometryUtil.cpp
  GfxPreview.cpp
  GroupingEntity.cpp
  LocalToleranceTool.cpp
  Loop.cpp
  LoopSM.cpp
  Lump.cpp
  LumpSM.cpp
  MedialTool2D.cpp
  MedialTool3D.cpp
  MergeTool.cpp
  MergeToolAssistant.cpp
  MidPlaneTool.cpp
  #ModelEntity.cpp
  ModelQueryEngine.cpp
  OffsetSplitTool.cpp
  OldUnmergeCode.cpp
  Point.cpp
  PointSM.cpp
  RefCollection.cpp
  RefEdge.cpp
  RefEntity.cpp
  RefEntityFactory.cpp
  RefEntityName.cpp
  RefFace.cpp
  RefGroup.cpp
  RefVertex.cpp
  RefVolume.cpp
  SenseEntity.cpp
  Shell.cpp
  ShellSM.cpp
  SphereEvaluator.cpp
  SplitSurfaceTool.cpp
  SurfParamTool.cpp
  Surface.cpp
  SurfaceOverlapFacet.cpp
  SurfaceOverlapTool.cpp
  SurfaceSM.cpp
  TBOwner.cpp
  TBOwnerSet.cpp
  TDCAGE.cpp
  TDSourceFeature.cpp
  TDSplitSurface.cpp
  TDSurfaceOverlap.cpp
  TDUniqueId.cpp
  TopologyBridge.cpp
  TopologyEntity.cpp)

set(extra_geom_srcs
  PeriodicParamTool.cpp
  AllocMemManagersGeom.cpp)

# Create a list of header-only files:
set(geom_headers
  cgm/CADefines.hpp
  cgm/CubitEvaluator.hpp
  cgm/DagType.hpp
  cgm/GeomPoint.hpp
  cgm/GeomSeg.hpp
  cgm/GeometryEvent.hpp
  cgm/GeometryFeatureEngine.hpp
  cgm/GeometryHealerEngine.hpp
  cgm/IntermediateGeomEngine.hpp
  cgm/MidPlaneTool.hpp
  cgm/TBOwner.hpp
  cgm/TBOwnerSet.hpp
  cgm/TDCompare.hpp
  # Generated files:
  "${CMAKE_CURRENT_BINARY_DIR}/cgm/CGMConfigure.h"
  "${CMAKE_CURRENT_BINARY_DIR}/cgm/CGMGeomConfigure.h")

set(CGM_GEOM_BUILD_SHARED_LIBS ${BUILD_SHARED_LIBS})
configure_file(
  "${CMAKE_SOURCE_DIR}/config/CGMGeomConfigure.h.in"
  "${CMAKE_CURRENT_BINARY_DIR}/cgm/CGMGeomConfigure.h"
  @ONLY)

configure_file(
  "${CMAKE_SOURCE_DIR}/config/CGMConfigure.h.in"
  "${CMAKE_CURRENT_BINARY_DIR}/cgm/CGMConfigure.h"
  @ONLY)

# Now algorithmically generate header names
# from implementation filenames:
foreach (src IN LISTS geom_srcs)
  string(REGEX REPLACE ".cpp" ".hpp" header "${src}")
  set(header "cgm/${header}")
  list(APPEND geom_headers
    "${header}")
endforeach ()

set(cgm_geom_target cgm_geom PARENT_SCOPE)
set(cgm_geom_target cgm_geom)
cgm_add_library(cgm_geom 
  ${geom_srcs}
  ${extra_geom_srcs})
set(cgm_geom_include_type PUBLIC)
if(CGM_LIBRARY_PROPERTIES)
  set_target_properties(cgm_geom
    PROPERTIES ${CGM_LIBRARY_PROPERTIES})
endif()

target_link_libraries(cgm_geom
  PUBLIC
    cgm_util)

cgm_install_headers(${geom_headers})

target_include_directories(cgm_geom
  ${cgm_geom_include_type}
    "$<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>/cgm"
    "$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>/cgm"
    "$<INSTALL_INTERFACE:include>")

# Make a library with the compat functions.
cgm_add_library(cgm_geom_compat STATIC
  cgm/CubitCompat.h
  cgm/CubitCompat.hpp
  CubitCompat.cpp)

target_link_libraries(cgm_geom_compat
  PUBLIC
    cgm_geom)

cgm_install_headers(
  cgm/CubitCompat.h
  cgm/CubitCompat.hpp)

add_subdirectory(Cholla)
add_subdirectory(facetbool)
add_subdirectory(facet)
add_subdirectory(virtual)

if (CGM_HAVE_OCC)
  add_subdirectory(OCC)
endif ()

if (CGM_HAVE_MPI)
  add_subdirectory(parallel)
endif ()
