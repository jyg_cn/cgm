project(cgm_facetbool)

set(facetbool_srcs
  FBClassify.cpp
  FBDataUtil.cpp
  FBImprint.cpp
  FBIntersect.cpp
  FBPolyhedron.cpp
  FBRetriangulate.cpp
  FBTiler.cpp
  IntegerHash.cpp
  KdTree.cpp)

set(facetbool_headers
  cgm/FBDefines.hpp
  cgm/FBStructs.hpp)

foreach (src IN LISTS facetbool_srcs)
  string(REGEX REPLACE ".cpp" ".hpp" header "${src}")
  list(APPEND facetbool_headers
    "cgm/${header}")
endforeach ()

cgm_source_interface(all_facetbool_srcs
  facetbool_srcs
  facetbool_headers)

add_library(cgm_facetboolstub INTERFACE)
target_sources(cgm_facetboolstub
  INTERFACE
    ${all_facetbool_srcs})
target_include_directories(cgm_facetboolstub
  INTERFACE
    "$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>/cgm"
    "$<INSTALL_INTERFACE:include>")
target_link_libraries(cgm_facetboolstub
  INTERFACE
    cgm_util)
cgm_install_headers(${facetbool_headers})

set_property(GLOBAL APPEND
  PROPERTY cgm_libs
  cgm_facetboolstub)
