# Don't require GNU-standard files (Changelog, README, etc.)
AUTOMAKE_OPTIONS = foreign

# Subdirectories
# The values checked are defined in AM_CONDITIONAL statements
# in configure.in
SUBDIRS = . cgm Cholla facetbool facet virtual

if build_parallel
  SUBDIRS += parallel
endif
if build_OCC
  SUBDIRS += OCC
endif

# Override default defines with the ones we want from the configure script
DEFS = $(TEMPLATE_DEFS_INCLUDED) $(LITTLE_ENDIAN)
AM_CPPFLAGS = -I. \
              -I$(top_srcdir)/src/util/cgm -I$(top_builddir)/src/util/cgm \
              -I$(top_builddir)/src/geom/cgm -I$(top_srcdir)/src/geom/cgm
# The name of the library to build
noinst_LTLIBRARIES = libcgm_geom.la

# The directory where headers will be installed
libcgm_geom_la_includedir = $(includedir)

# The non-template sources
if BUILD_CGM
libcgm_geom_la_SOURCES = \
    AllocMemManagersGeom.cpp \
    AnalyticGeometryTool.cpp \
    AutoMidsurfaceTool.cpp \
    BasicTopologyEntity.cpp \
    Body.cpp \
    BodySM.cpp \
    BoundingBoxTool.cpp \
    BridgeManager.cpp \
    CAActuateSet.cpp \
    CADeferredAttrib.cpp \
    CAEntityColor.cpp \
    CAEntityId.cpp \
    CAEntityName.cpp \
    CAEntityTol.cpp \
    CAEntitySense.cpp \
    CAGroup.cpp \
    CAMergePartner.cpp \
    CAMergeStatus.cpp \
    CASourceFeature.cpp \
    CAUniqueId.cpp \
    CGMApp.cpp \
    CGMEngineDynamicLoader.cpp \
    CGMHistory.cpp \
    Chain.cpp \
    CoEdge.cpp \
    CoEdgeSM.cpp \
    CoFace.cpp \
    CollectionEntity.cpp \
    CoVertex.cpp \
    CoVolume.cpp \
    CubitAttrib.cpp \
    CubitAttribManager.cpp \
    CubitAttribUser.cpp \
    CubitCompat.cpp \
    CubitPolygon.cpp \
    CubitSimpleAttrib.cpp \
    Curve.cpp \
    CurveOverlapFacet.cpp \
    CurveSM.cpp \
    CylinderEvaluator.cpp \
    DAG.cpp \
    DagDrawingTool.cpp \
    GeomDataObserver.cpp \
    GeometryEntity.cpp \
    GeometryEvent.cpp \
    GeometryFeatureEngine.cpp \
    GeometryFeatureTool.cpp \
    GeometryHealerEngine.cpp \
    GeometryHealerTool.cpp \
    GeometryModifyEngine.cpp \
    GeometryModifyTool.cpp \
    GeometryQueryEngine.cpp \
    GeometryQueryTool.cpp \
    GeometryUtil.cpp \
    GeomMeasureTool.cpp \
    GfxPreview.cpp \
    GroupingEntity.cpp \
    GSaveOpen.cpp \
    LocalToleranceTool.cpp \
    Loop.cpp \
    LoopSM.cpp \
    Lump.cpp \
    LumpSM.cpp \
    MedialTool2D.cpp \
    MedialTool3D.cpp \
    MergeToolAssistant.cpp \
    MergeTool.cpp \
    MidPlaneTool.cpp \
    ModelQueryEngine.cpp \
    OffsetSplitTool.cpp \
    OldUnmergeCode.cpp \
    Point.cpp \
    PointSM.cpp \
    RefCollection.cpp \
    RefEdge.cpp \
    RefEntity.cpp \
    RefEntityFactory.cpp \
    RefEntityName.cpp \
    RefFace.cpp \
    RefGroup.cpp \
    RefVertex.cpp \
    RefVolume.cpp \
    SenseEntity.cpp \
    Shell.cpp \
    ShellSM.cpp \
    SphereEvaluator.cpp \
    SplitSurfaceTool.cpp \
    Surface.cpp \
    SurfaceOverlapFacet.cpp \
    SurfaceOverlapTool.cpp \
    SurfParamTool.cpp \
    SurfaceSM.cpp \
    TBOwner.cpp \
    TBOwnerSet.cpp \
    TDCAGE.cpp \
    TDSourceFeature.cpp \
    TDSplitSurface.cpp \
    TDSurfaceOverlap.cpp \
    TDUniqueId.cpp \
    TopologyBridge.cpp \
    TopologyEntity.cpp 
else
    libcgm_geom_la_SOURCES = CubitCompat.cpp
endif

# Headers to be installed.  If any file in this list should
# not be installed, move it to the _SOURCES list above.
nobase_libcgm_geom_la_include_HEADERS = \
  cgm/AnalyticGeometryTool.hpp \
  cgm/AutoMidsurfaceTool.hpp \
  cgm/BasicTopologyEntity.hpp \
  cgm/Body.hpp \
  cgm/BodySM.hpp \
  cgm/BoundingBoxTool.hpp \
  cgm/BridgeManager.hpp \
  cgm/CAActuateSet.hpp \
  cgm/CADeferredAttrib.hpp \
  cgm/CADefines.hpp \
  cgm/CAEntityColor.hpp \
  cgm/CAEntityId.hpp \
  cgm/CAEntityName.hpp \
  cgm/CAEntityTol.hpp \
  cgm/CAEntitySense.hpp \
  cgm/CAGroup.hpp \
  cgm/CAMergePartner.hpp \
  cgm/CAMergeStatus.hpp \
  cgm/CASourceFeature.hpp \
  cgm/CAUniqueId.hpp \
  cgm/CGMApp.hpp \
  cgm/CGMEngineDynamicLoader.hpp \
  cgm/CGMHistory.hpp \
  cgm/Chain.hpp \
  cgm/CoEdge.hpp \
  cgm/CoEdgeSM.hpp \
  cgm/CoFace.hpp \
  cgm/CoVertex.hpp \
  cgm/CoVolume.hpp \
  cgm/CollectionEntity.hpp \
  cgm/CubitAttrib.hpp \
  cgm/CubitAttribManager.hpp \
  cgm/CubitAttribUser.hpp \
  cgm/CubitCompat.hpp \
  cgm/CubitCompat.h \
  cgm/CubitEvaluator.hpp \
  cgm/CubitPolygon.hpp \
  cgm/CubitSimpleAttrib.hpp \
  cgm/Curve.hpp \
  cgm/CurveOverlapFacet.hpp \
  cgm/CurveSM.hpp \
  cgm/CylinderEvaluator.hpp \
  cgm/DAG.hpp \
  cgm/DagDrawingTool.hpp \
  cgm/DagType.hpp \
  cgm/GSaveOpen.hpp \
  cgm/GeomDataObserver.hpp \
  cgm/GeomMeasureTool.hpp \
  cgm/GeomPoint.hpp \
  cgm/GeomSeg.hpp \
  cgm/GeometryEntity.hpp \
  cgm/GeometryFeatureEngine.hpp \
  cgm/GeometryFeatureTool.hpp \
  cgm/GeometryHealerEngine.hpp \
  cgm/GeometryHealerTool.hpp \
  cgm/GeometryModifyEngine.hpp \
  cgm/GeometryModifyTool.hpp \
  cgm/GeometryQueryEngine.hpp \
  cgm/GeometryQueryTool.hpp \
  cgm/GeometryUtil.hpp \
  cgm/GfxPreview.hpp \
  cgm/GroupingEntity.hpp \
  cgm/IntermediateGeomEngine.hpp \
  cgm/LocalToleranceTool.hpp \
  cgm/Loop.hpp \
  cgm/LoopSM.hpp \
  cgm/Lump.hpp \
  cgm/LumpSM.hpp \
  cgm/MedialTool2D.hpp \
  cgm/MedialTool3D.hpp \
  cgm/MergeTool.hpp \
  cgm/MergeToolAssistant.hpp \
  cgm/MidPlaneTool.hpp \
  cgm/ModelQueryEngine.hpp \
  cgm/OffsetSplitTool.hpp \
  cgm/OldUnmergeCode.hpp \
  cgm/Point.hpp \
  cgm/PointSM.hpp \
  cgm/RefCollection.hpp \
  cgm/RefEdge.hpp \
  cgm/RefEntity.hpp \
  cgm/RefEntityFactory.hpp \
  cgm/RefEntityName.hpp \
  cgm/RefFace.hpp \
  cgm/RefGroup.hpp \
  cgm/RefVertex.hpp \
  cgm/RefVolume.hpp \
  cgm/SenseEntity.hpp \
  cgm/Shell.hpp \
  cgm/ShellSM.hpp \
  cgm/SphereEvaluator.hpp \
  cgm/SplitSurfaceTool.hpp \
  cgm/SurfParamTool.hpp \
  cgm/Surface.hpp \
  cgm/SurfaceOverlapFacet.hpp \
  cgm/SurfaceOverlapTool.hpp \
  cgm/SurfaceSM.hpp \
  cgm/TBOwner.hpp \
  cgm/TBOwnerSet.hpp \
  cgm/TDCAGE.hpp \
  cgm/TDCompare.hpp \
  cgm/TDSourceFeature.hpp \
  cgm/TDSplitSurface.hpp \
  cgm/TDSurfaceOverlap.hpp \
  cgm/TDUniqueId.hpp \
  cgm/TopologyBridge.hpp \
  cgm/TopologyEntity.hpp \
  cgm/GeometryEvent.hpp

nobase_includedir = $(includedir)/cgm
nodist_nobase_include_HEADERS = cgm/CGMGeomConfigure.h

DISTCLEANFILES = cgm/CGMGeomConfigure.h
